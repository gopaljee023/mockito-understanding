package com.example.gopaljee.learningmvp;

import com.example.gopaljee.learningmvp.model.Book;
import com.example.gopaljee.learningmvp.respositiries.IBookRespository;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

import static org.junit.Assert.*;



/**
 * Created by gopaljee on 04/07/17.
 */


@RunWith(MockitoJUnitRunner.class)
public class BookActivityPresenterTest {

    @Mock
    IBookRespository booksRespository;

    @Mock
    IBookAcitivityView view;

    private BookActivityPresenter presenter;
    private final List<Book> booklist = Arrays.asList(new Book(), new Book(), new Book());
    @Before
    public void setup()
    {
        presenter = new BookActivityPresenter(view, booksRespository);
    }


    @Test
    public void shouldHandleNoBooks() {
        //given
        Mockito.when(this.booksRespository.getBooks()).thenReturn(Collections.EMPTY_LIST);

        //when

        presenter.loadBooks();

        //then
        Mockito.verify(view).displayNoBookMessage();
    }

    @Test
    public void shouldPassBooksToView() {
        //given

        Mockito.when(this.booksRespository.getBooks()).thenReturn(booklist);


        //when
        BookActivityPresenter presenter = new BookActivityPresenter(view, booksRespository);
        presenter.loadBooks();

        //then
        Mockito.verify(view).displayBooks(booklist);
    }

    @Test
    public void shouldHandleException()
    {
        //given
        Mockito.when(this.booksRespository.getBooks()).thenThrow(new RuntimeException("boom!"));

        //when
        presenter.loadBooks();

        //then
        Mockito.verify(view).displayError();


    }
}

/*public class BookActivityPresenterTest {

    @Test
    public void shouldHandleNoBooks()
    {
        //given
        IBookAcitivityView view = new MockView();
        IBookRespository respository = new MockBookRepository(false);

        //when
        BookActivityPresenter presenter = new BookActivityPresenter(view,respository);
        presenter.loadBooks();

        //then
        assertEquals(true, ((MockView)view).noBookDisplayed);
    }

    @Test
    public void shouldPassBooksToView()
    {
        //given
        IBookAcitivityView view = new MockView();
        IBookRespository respository = new MockBookRepository(true);

        //when
        BookActivityPresenter presenter = new BookActivityPresenter(view,respository);
        presenter.loadBooks();

        //then
        assertEquals(true, ((MockView)view).bookDisplayed);
    }


    private class MockView  implements  IBookAcitivityView{
        boolean noBookDisplayed;
        @Override
        public void displayNoBookMessage() {
            noBookDisplayed  = true;
        }

        boolean bookDisplayed;
        @Override
        public void displayBooks(List<Book> bookList) {
            if(bookList.size() ==3)
                bookDisplayed = true;

        }
    }

    //book repsository can return books, no books, or throw exception.
    private class MockBookRepository implements IBookRespository
    {

        private boolean mIsBooks = false;
        public MockBookRepository(boolean isBooks)
        {
            mIsBooks = isBooks;
        }
        @Override
        public List<Book> getBooks() {
            if(mIsBooks==true)
            return Arrays.asList(new Book(),new Book(),new Book());
            else
                return Collections.emptyList();
        }
    }

}*/
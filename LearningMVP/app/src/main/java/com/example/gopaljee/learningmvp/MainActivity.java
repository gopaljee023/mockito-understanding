package com.example.gopaljee.learningmvp;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Toast;

import com.example.gopaljee.learningmvp.model.Book;

import java.util.List;

public class MainActivity extends AppCompatActivity implements IBookAcitivityView {
    @Override
    public void displayError() {
        //Toast + tab
        Toast.makeText(this, "Some went wrong", Toast.LENGTH_SHORT).show();
    }

    BookActivityPresenter presenter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        presenter = new BookActivityPresenter(this, null);
    }

    @Override
    public void displayNoBookMessage() {

    }

    @Override
    public void displayBooks(List<Book> bookList) {

    }
}

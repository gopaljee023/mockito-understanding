package com.example.gopaljee.learningmvp;

import com.example.gopaljee.learningmvp.model.Book;

import java.util.List;

/**
 * Created by gopaljee on 04/07/17.
 */

public interface IBookAcitivityView {

    void displayBooks(List<Book> bookList);
    void displayNoBookMessage();

    void displayError();
}

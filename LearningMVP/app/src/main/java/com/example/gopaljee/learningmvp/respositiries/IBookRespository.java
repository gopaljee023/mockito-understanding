package com.example.gopaljee.learningmvp.respositiries;

import com.example.gopaljee.learningmvp.model.Book;

import java.util.List;

/**
 * Created by gopaljee on 04/07/17.
 */

public interface IBookRespository {

    List<Book> getBooks();
}

package com.example.gopaljee.learningmvp.model;

/**
 * Created by gopaljee on 04/07/17.
 */

public class Book {

    String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}

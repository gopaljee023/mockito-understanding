package com.example.gopaljee.learningmvp;

import com.example.gopaljee.learningmvp.model.Book;
import com.example.gopaljee.learningmvp.respositiries.IBookRespository;

import java.util.List;

/**
 * Created by gopaljee on 04/07/17.
 */

public class BookActivityPresenter {


    private final IBookAcitivityView mView;
    IBookRespository mRespository;

    public BookActivityPresenter(IBookAcitivityView view, IBookRespository respository) {
        mView = view;
        mRespository = respository;
    }

    public void loadBooks() {
        try {
            List<Book> bookList = mRespository.getBooks();
            if (bookList.size() > 0)
                mView.displayBooks(bookList);
            else
                mView.displayNoBookMessage();
        }
        catch (Exception e)
        {
            mView.displayError();
        }
    }
}
